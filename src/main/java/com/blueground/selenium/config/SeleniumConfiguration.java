package com.blueground.selenium.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class SeleniumConfiguration {

	@Autowired
	private Environment env;
	
	@PostConstruct
	public void init() {
		System.setProperty(env.getProperty("webdriver"), env.getProperty("destination"));
	}
}
