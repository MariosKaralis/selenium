package com.blueground.selenium.spitogatos;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Component
public class SpitogatosSelenium {
	
	public void spitogatosAdvsVipUp(WebDriver driver, JavascriptExecutor jse, Actions actions) throws Exception {
		driver.get("https://www.spitogatos.gr/");
		login(driver, actions);
		if (checkForAutomaticReconnection(driver, "//*[@id='form-login']/p/span")) {
			clickXpath(driver, actions, "//*[@id='form-login']/p/span");
			// click connection
			clickXpath(driver, actions, "//*[@id='form-login']/button");
		} else {
			clickXpath(driver, actions, "//*[@id='form-login']/button");
		}
		// property adverts
		clickXpath(driver, actions, "//*[@id='shortcuts']/li[2]");
		Thread.sleep(8000);
		// projected VIP and UP
		clickXpath(driver, actions, "//*[@id='mainContent']/div[1]/div/ul/li[2]");
		// scroll down
		jse.executeScript("scroll(0, 400)");
		clickXpath(driver, actions, "//*[@id='mynewbutton1']/span[2]"); // research
		Thread.sleep(8000);
		// number of free spaces for drag and drop
		Integer freePropertySpaces = getFreeSpaces(driver, "//*[@id='descriptionText']/div[1]/strong[3]")
				+ getFreeSpaces(driver, "//*[@id='descriptionText']/div[2]/strong[3]");
		if (freePropertySpaces.equals(0)) {
			return;
		}
		// get ids from spitogatos dashboard
		scrollDown(driver, actions, "//*[@id='guruHeaderDateAndDataWrapper']/div[2]/strong[1]/span");
		Map<String, Integer> spitogatosPropIds = getAllPropertyIds(driver);
		JsonArray idsFromFile = inputFileWithIds(new File("src/main/resources/jsons/json.txt"));
		if (idsFromFile.isJsonNull()) {
			return;
		}
		// click select-arrow for free prop spaces
		jse.executeScript("scroll(0, -400)"); // scroll up
		clickXpath(driver, actions, "//*[@id='featuredContainment']/div[4]/div[2]/span[1]");
		// click free spaces
		clickXpath(driver, actions, "/html/body/span/span[3]/span[3]");
		// scroll down again
		jse.executeScript("scroll(0, 400)");
		if (!spitogatosPropIds.isEmpty()) {
			Integer freeSpaceCounter = 1;
			for (int j = 0; j < idsFromFile.size(); j++) {
				if (freePropertySpaces.equals(0))
					break;
				String id = idsFromFile.get(j).getAsString();
				if (!spitogatosPropIds.containsKey(id)) {
					continue;
				}
				if (checkVipUpIfExists(driver, id))
					continue;
				dragAndDrop(driver, actions, "//*[@id='listing_" + id + "']/div/p",
						"//*[@id='featuredContainment']/div[4]/div[3]/div[" + freeSpaceCounter.toString()
								+ "]/div[1]");
				freeSpaceCounter++;
			}
		}
	}
	
	private void login(WebDriver driver, Actions actions) {
		clickXpath(driver, actions, "//*[@id='around']/div[1]/header/div[2]/div[3]/div[2]/div[1]/a[2]");
		textFieldId(driver, "fieldEmail", "blueground");
		textFieldId(driver, "fieldPassword", "cuberealestate");
	}
	
	private Boolean checkForAutomaticReconnection(WebDriver driver, String xpath) {
		Boolean autoStatus = false;
		WebElement checkboxChecked = driver.findElement(By.xpath(xpath));
		if (checkboxChecked.isEnabled()) {
			autoStatus = true;
		}
		return autoStatus;
	}
	
	private void clickXpath(WebDriver driver, Actions actions, String xPath) {
		actions.moveToElement(driver.findElement(By.xpath(xPath))).click().perform();
	}

	private void textFieldId(WebDriver driver, String id, String value) {
		driver.findElement(By.id(id)).clear();
		driver.findElement(By.id(id)).sendKeys(value);
	}

	private Integer getFreeSpaces(WebDriver driver, String xpath) {
		String text = (driver.findElement(By.xpath(xpath))).getText();
		return Integer.valueOf(text);
	}
	
	private void scrollDown(WebDriver driver, Actions actions, String xpath) {

		String advsCont = driver.findElement(By.xpath(xpath)).getText();
		Integer numOfAdvs = Integer.parseInt(advsCont);
		for (int i = 10; i < numOfAdvs; i += 10) {
			try {
				clickXpath(driver, actions, "//*[@id='scrollBox']/div[" + String.valueOf(i) + "]");
				Thread.sleep(500);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private Map<String, Integer> getAllPropertyIds(WebDriver driver) {
		Integer index = 1;
		WebElement selectedContainer = driver.findElement(By.xpath("//*[@id='featuredContainment']/div[6]/div/div[3]"));
		WebElement scrollBox = selectedContainer.findElement(By.xpath("//*[@id='scrollBox']"));
		Map<String, Integer> propIds = new HashMap<String, Integer>();
		List<WebElement> selectedSlots = scrollBox.findElements(By.className("selectedSlot"));

		for (WebElement boldElement : selectedSlots) {
			String boldElementStr = boldElement.getText();
			if (StringUtils.isBlank(boldElementStr)) {
				continue;
			}
			Integer boldElementPos = boldElementStr.indexOf("\n");
			String id = StringUtils.trim(boldElementStr.substring(1, boldElementPos));
			if (StringUtils.isBlank(id)) {
				continue;
			}
			propIds.put(id, index);
			index++;
		}
		return propIds;
	}
	private JsonArray inputFileWithIds(File file) throws IOException {
		String jsonTxt = FileUtils.readFileToString(file);
		JsonParser parser = new JsonParser();
		JsonObject jsonTxtObj = (JsonObject) parser.parse(jsonTxt);
		JsonArray jsonTxtArray = null;
		if (jsonTxtObj.has("ids")) {
			jsonTxtArray = jsonTxtObj.get("ids") != null ? jsonTxtObj.get("ids").getAsJsonArray() : new JsonArray();
		}
		return jsonTxtArray;
	}

	private Boolean checkVipUpIfExists(WebDriver driver, String id) {
		Boolean isExisting = false;
		WebElement selectedContainer = driver.findElement(By.xpath("//*[@id='featuredContainment']/div[6]/div/div[3]"));
		WebElement scrollBox = selectedContainer.findElement(By.xpath("//*[@id='scrollBox']"));
		WebElement slotInfo = scrollBox.findElement(By.className("slotInfo"));
		Integer indexOfPropId = getAllPropertyIds(driver).get(id);
		List<WebElement> slotDescription = slotInfo.findElements(
				By.xpath("//*[@id='scrollBox']/div[" + String.valueOf(indexOfPropId) + "]/div[2]/div/div[3]"));
		WebElement vipFeatured = scrollBox.findElement(By.className("vip_featured"));
		WebElement upFeatured = scrollBox.findElement(By.className("up_featured"));
		if (slotDescription.contains(vipFeatured)) {
			isExisting = true;
		} else if (slotDescription.contains(upFeatured)) {
			isExisting = true;
		}
		return isExisting;
	}

	private void dragAndDrop(WebDriver driver, Actions builder, String xPathFrom, String xPathTo) {
		WebElement from = driver.findElement(By.xpath(xPathFrom));
		WebElement to = driver.findElement(By.xpath(xPathTo));
		builder = new Actions(driver);
		Action dragDrop = builder.clickAndHold(from).moveToElement(to).release(to).build();
		dragDrop.perform();
	}

}
