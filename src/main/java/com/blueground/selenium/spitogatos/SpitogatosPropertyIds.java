package com.blueground.selenium.spitogatos;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class SpitogatosPropertyIds {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({ "ids" })
	@JsonProperty("ids")
	private List<String> ids = new ArrayList<String>();

	@JsonProperty("ids")
	public List<String> getIds() {
		return ids;
	}

	@JsonProperty("ids")
	public void setIds(List<String> ids) {
		this.ids = ids;
	}

}
