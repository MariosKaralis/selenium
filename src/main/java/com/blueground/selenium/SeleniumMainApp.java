package com.blueground.selenium;

import java.util.Locale;
import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@EntityScan("com.blueground.selenium")
@ComponentScan("com.blueground.selenium")
@Configuration
@PropertySource(value = "file:./application.configuration")
public class SeleniumMainApp {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		SpringApplication.run(SeleniumMainApp.class, args);
	}

	
}
