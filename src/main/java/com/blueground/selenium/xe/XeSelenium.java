package com.blueground.selenium.xe;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.springframework.stereotype.Component;

@Component
public class XeSelenium {
	
	public void xeAdvsVipUp(WebDriver driver, JavascriptExecutor jse, Actions actions) {
		driver.get("http://www.xe.gr/");
		login(driver, actions);
		if (checkForAutoLogin(driver, "//*[@id='autologin']")) {
			clickXpath(driver, actions, "//*[@id='autologin']");
			// click connection
			clickXpath(driver, actions, "//*[@id='box_login']/form/p[2]/a");
		} else {
			clickXpath(driver, actions, "//*[@id='box_login']/form/p[2]/a");
		}

	}
	
	private void login(WebDriver driver, Actions actions)  {
		// for professionals
		clickXpath(driver,actions,"//*[@id='xe_network_account']/li[2]/a");
		textFieldId(driver,"email","info@cuberealestate.gr");
		textFieldId(driver,"password","adderos123");
	}
	
	private Boolean checkForAutoLogin(WebDriver driver, String xpath) {
		Boolean autoLogin = false;
		WebElement checkboxChecked = driver.findElement(By.xpath(xpath));
		if (checkboxChecked.isEnabled()) {
			autoLogin = true;
		}
		return autoLogin;
	}

	private void clickXpath(WebDriver driver, Actions actions, String xPath) {
		actions.moveToElement(driver.findElement(By.xpath(xPath))).click().perform();
	}
	
	private void textFieldId(WebDriver driver, String id, String value) {
		driver.findElement(By.id(id)).clear();
		driver.findElement(By.id(id)).sendKeys(value);
	}


}
