package com.blueground.selenium.controller;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.blueground.selenium.spitogatos.SpitogatosSelenium;
import com.blueground.selenium.xe.XeSelenium;
import com.blueground.selenium.spitogatos.SpitogatosPropertyIds;

@RestController
@RequestMapping("/selenium")
public class SeleniumController {
	private final Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private DriverManager driverManager;

	@Autowired
	private SpitogatosSelenium spitogatosSelenium;
	
	@Autowired
	private XeSelenium xeSelenium;

	@RequestMapping(value = "/spitogatosAdvs", method = {RequestMethod.POST})
	public void spitogatos(@RequestBody SpitogatosPropertyIds propIds) {
		
		// initializr driver
		WebDriver driver = driverManager.initializeWebDriver();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		Actions actions = new Actions(driver);
		
		// open spitogatos gr
		try {
			spitogatosSelenium.spitogatosAdvsVipUp(driver, jse, actions);
		} catch (Exception e) {
			logger.error("Failed to execute spitogatos", e);
		} finally {
			driverManager.closeWebDriver(driver);
		}
	}
	
	@RequestMapping(value = "/xeAdvs", method = { RequestMethod.POST })
	public void xe() {

		WebDriver driver = driverManager.initializeWebDriver();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		Actions actions = new Actions(driver);

		try {
			xeSelenium.xeAdvsVipUp(driver, jse, actions);
		} catch (Exception e) {
			logger.error("Failed to execute xe", e);
		} finally {
			driverManager.closeWebDriver(driver);
		}
	}
	
}
